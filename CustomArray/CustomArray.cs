﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CustomArray
{
    public class CustomArray<T> : IEnumerable<T>
    {
        /// <summary>
        /// Should return first index of array
        /// </summary>
        public int First 
        { 
            get;
            private set;
        }

        /// <summary>
        /// Should return last index of array
        /// </summary>
        public int Last 
        { 
            get=> First+Length-1;
        }

        /// <summary>
        /// Should return length of array
        /// <exception cref="ArgumentException">Thrown when value was smaller than 0</exception>
        /// </summary>
        public int Length 
        {
            get { return Array.Length; }
            private set
            {
                if (value < 0)
                {
                    throw new ArgumentException("value was smaller than 0");
                }
            }

        }

        /// <summary>
        /// Should return array 
        /// </summary>
        public T[] Array
        {
            get;
        }


        /// <summary>
        /// Constructor with first index and length
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="length">Length</param>
        public CustomArray(int first, int length)
        {
            Array = new T[length];
            Length = length;
            First = first;
        }


        /// <summary>
        /// Constructor with first index and collection  
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="list">Collection</param>
        ///  <exception cref="ArgumentNullException">Thrown when list is null</exception>
        public CustomArray(int first, IEnumerable<T> list)
        {
            if (list == null)
                throw new ArgumentNullException("list is null");
            Array = list.ToArray();
            Length = Array.Length;
            First = first;
        }

        /// <summary>
        /// Constructor with first index and params
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="list">Params</param>
        ///  <exception cref="ArgumentNullException">Thrown when list is null</exception>
        /// <exception cref="ArgumentException">Thrown when list without elements </exception>
        public CustomArray(int first, params T[] list)
        {
            if (list == null)
                throw new ArgumentNullException("list is null");
            if (!list.Any())
                throw new ArgumentNullException("list without elements");
            Array = list;
            Length = Array.Length;
            First = first;

        }

        /// <summary>
        /// Indexer with get and set  
        /// </summary>
        /// <param name="item">Int index</param>        
        /// <returns></returns>
        /// <exception cref="ArgumentException">Thrown when index out of array range</exception> 
        /// <exception cref="ArgumentNullException">Thrown in set  when value passed in indexer is null</exception>
        public T this[int item]
        {
            get
            {
                if (item < First || item >= First + Length)
                    throw new ArgumentException("index out of array range");
                return Array[item - First];
            }
            set
            {
                if (item < First || item >= First + Length)
                    throw new ArgumentException("index out of array range");
                if (value == null)
                    throw new ArgumentNullException("value passed in indexer is null");
                Array[item - First] = value;
            }

        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int index = 0; index < Length; index++)
                yield return Array[index];

        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
